set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'commits_by_author.png'
set key left top
set yrange [0:]
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Commits"
set xtics rotate
set bmargin 6
plot 'commits_by_author.dat' using 1:2 title "Lars Nørregaard" w lines, 'commits_by_author.dat' using 1:3 title "Bram van der Giessen" w lines, 'commits_by_author.dat' using 1:4 title "Peter Štibraný" w lines, 'commits_by_author.dat' using 1:5 title "Niels Hagelberg" w lines, 'commits_by_author.dat' using 1:6 title "Ayoub Hajjem" w lines, 'commits_by_author.dat' using 1:7 title "Thomas Bonnesen" w lines, 'commits_by_author.dat' using 1:8 title "Søren Tørnkvist" w lines, 'commits_by_author.dat' using 1:9 title "Kristian Sørensen" w lines, 'commits_by_author.dat' using 1:10 title "Tomasz Stalka" w lines, 'commits_by_author.dat' using 1:11 title "Robin" w lines, 'commits_by_author.dat' using 1:12 title "Oleksandr Levytskyy" w lines, 'commits_by_author.dat' using 1:13 title "Martyna Gajaszek-Hansen" w lines, 'commits_by_author.dat' using 1:14 title "Kristian Grønfeldt Sørensen" w lines, 'commits_by_author.dat' using 1:15 title "tbonnesen" w lines, 'commits_by_author.dat' using 1:16 title "mreib" w lines
