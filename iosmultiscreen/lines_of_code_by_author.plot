set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'lines_of_code_by_author.png'
set key left top
set yrange [0:]
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Lines"
set xtics rotate
set bmargin 6
plot 'lines_of_code_by_author.dat' using 1:2 title "Anders Friis Pedersen" w lines, 'lines_of_code_by_author.dat' using 1:3 title "Nicolaj Roos" w lines, 'lines_of_code_by_author.dat' using 1:4 title "Anders Pedersen" w lines, 'lines_of_code_by_author.dat' using 1:5 title "Mads Vium Asbjoern" w lines, 'lines_of_code_by_author.dat' using 1:6 title "Vladimir Sánchez Mondeja" w lines, 'lines_of_code_by_author.dat' using 1:7 title "Athina" w lines, 'lines_of_code_by_author.dat' using 1:8 title "Vladimír Sanchéz Mondeja" w lines, 'lines_of_code_by_author.dat' using 1:9 title "Jesper Jepsen" w lines, 'lines_of_code_by_author.dat' using 1:10 title "Dorinel Panaite" w lines, 'lines_of_code_by_author.dat' using 1:11 title "Tue Malthesen" w lines, 'lines_of_code_by_author.dat' using 1:12 title "Min Wu" w lines, 'lines_of_code_by_author.dat' using 1:13 title "Athina Tsagkari" w lines, 'lines_of_code_by_author.dat' using 1:14 title "Anjello S" w lines, 'lines_of_code_by_author.dat' using 1:15 title "jej" w lines, 'lines_of_code_by_author.dat' using 1:16 title "Vladimir Sánchez Mondeja" w lines, 'lines_of_code_by_author.dat' using 1:17 title "Bram van der Giessen" w lines, 'lines_of_code_by_author.dat' using 1:18 title "William Mendis" w lines, 'lines_of_code_by_author.dat' using 1:19 title "Thomas Bonnesen" w lines, 'lines_of_code_by_author.dat' using 1:20 title "Robin" w lines, 'lines_of_code_by_author.dat' using 1:21 title "Anjello Sampath Wimalachandra" w lines, 'lines_of_code_by_author.dat' using 1:22 title "Robin Zernickov" w lines, 'lines_of_code_by_author.dat' using 1:23 title "mckenzie-ci" w lines, 'lines_of_code_by_author.dat' using 1:24 title "Mads Vium Asbjørn" w lines, 'lines_of_code_by_author.dat' using 1:25 title "Mackenzie" w lines, 'lines_of_code_by_author.dat' using 1:26 title "Mckenzie CI" w lines, 'lines_of_code_by_author.dat' using 1:27 title "Thomas Christensen" w lines
