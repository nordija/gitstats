set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'commits_by_author.png'
set key left top
set yrange [0:]
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Commits"
set xtics rotate
set bmargin 6
plot 'commits_by_author.dat' using 1:2 title "Claus Weng" w lines, 'commits_by_author.dat' using 1:3 title "mciglan" w lines, 'commits_by_author.dat' using 1:4 title "rstangel" w lines, 'commits_by_author.dat' using 1:5 title "jhuba" w lines, 'commits_by_author.dat' using 1:6 title "Thomas Bonnesen" w lines, 'commits_by_author.dat' using 1:7 title "Lars Nørregaard" w lines, 'commits_by_author.dat' using 1:8 title "Tue Malthesen" w lines, 'commits_by_author.dat' using 1:9 title "Dorinel Panaite" w lines, 'commits_by_author.dat' using 1:10 title "Lars M. Nørregaard" w lines, 'commits_by_author.dat' using 1:11 title "cweng" w lines, 'commits_by_author.dat' using 1:12 title "Kristian Sørensen" w lines, 'commits_by_author.dat' using 1:13 title "Mikael Dalkvist" w lines, 'commits_by_author.dat' using 1:14 title "Kristian Grønfeldt Sørensen" w lines
